;;; polylith.el --- Mode for interacting with the polylith tool -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2023 Chad Walstrom
;;
;; Author: Chad Walstrom <chewie@wookimus.net>
;; Maintainer: Chad Walstrom <chewie@wookimus.net>
;; Created: February 27, 2023
;; Modified: February 27, 2023
;; Version: 0.0.1
;; Keywords: clojure faces languages lisp local tools polylith
;; Homepage: https://github.com/chewie/polylith
;; Package-Requires: ((emacs "24.3"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Mode for interacting with the polylith tool
;;
;;; Code:



(provide 'polylith)

(defgroup polylith nil
  "Tools for running Polylith"
  :group 'tools)

(defvar polylith-cli-command "clojure"
  "Path to the program used by `polylith-shell'")

(defvar polylith-cli-arguments '("-M:poly" "shell")
  "Commandline arguments to pass to `polylith'.")

(defvar polylith-mode-map
  (let ((map (nconc (make-sparse-keymap) comint-mode-map)))
    ;; example definition
    (define-key map "\t" 'self-insert-command)
    map)
  "Basic mode map for `polylith-shell'.")

(defvar polylith-prompt-regexp "^\\(?:[a-zA-Z0-9./_-]+\\$\\)"
  "Prompt for `polylith-shell'.")

(defvar polylith-buffer-name "*Polylith*"
  "Name of the buffer to use for the `polylith-shell' comint instance.")

(defun polylith-shell ()
  "Run an inferior instance of `polylith' inside Emacs."
  (interactive)
  (let* ((polylith-program polylith-cli-command)
         (buffer (get-buffer-create polylith-buffer-name))
         (proc-alive (comint-check-proc buffer))
         (process (get-buffer-process buffer)))
    ;; if the process is dead then re-create the process and reset the
    ;; mode.
    (unless proc-alive
      (with-current-buffer buffer
        (apply 'make-comint-in-buffer "Polylith" buffer
               polylith-program nil polylith-cli-arguments)
        (polylith-mode)))
    ;; Regardless, provided we have a valid buffer, we pop to it.
    (when buffer
      (pop-to-buffer buffer))))

(defun polylith--initialize ()
  "Helper function to initialize Polylith."
  (setq comint-process-echoes t)
  (setq comint-use-prompt-regexp t))

(define-derived-mode polylith-mode comint-mode "Polylith"
  "Major mode for `polylith-shell'.

\\<polylith-mode-map>"
  ;; this sets up the prompt so it matches things like: project-title$
  (setq comint-prompt-regexp polylith-prompt-regexp)
  (setq comint-prompt-read-only t)
  ;; this makes it so commands like M-{ and M-} work.
  (set (make-local-variable 'paragraph-separate) "\\'")
  (set (make-local-variable 'font-lock-defaults) '(polylith-font-lock-keywords t))
  (set (make-local-variable 'paragraph-start) polylith-prompt-regexp))

(add-hook 'polylith-mode-hook 'polylith--initialize)

(defconst polylith-keywords
  '("base" "c" "check" "create" "deps" "diff" "exit" "help" "info" "libs"
    "project" "quit" "shell" "tap" "test" "version" "workspace" "ws")
  "List of keywords to highlight in `polylith-font-lock-keywords'.")

(defvar polylith-font-lock-keywords
  (list
   ;; highlight all the reserved commands.
   `(,(concat "\\_<" (regexp-opt polylith-keywords) "\\_>") . font-lock-keyword-face))
  "Additional expressions to highlight in `polylith-mode'.")

;;; polylith.el ends here
